# REDENV

Versioned secrets, in Rust

## Redenv API

The Redenv API allows you to store secrets in a versioned manner, organized by namespaces. Secrets can be created, retrieved, and deleted using the provided endpoints.

### Base URL

The base URL for the Redenv API depends on the deployment:

- Online Service: `https://127.0.0.1:4000`
- Local Deployment (UNIX socket): `unix:/tmp/redenv.sock`

### Endpoints

#### 1. Retrieve a Secret

Retrieve a specific version of a secret stored in a namespace.

**Endpoint:** `GET /redenv/<namespace>/<key>:<version>`

- `<namespace>`: The namespace in which the secret is stored.
- `<key>`: The key or name of the secret.
- `<version>` (optional): The version of the secret to retrieve. If omitted, the API will return the latest version (progressive number starting from 0).

**Example:**

```
GET /redenv/my_namespace/my_secret:2
```

This request will retrieve version 2 of the secret named `my_secret` in the `my_namespace` namespace.

#### 2. Create a Secret

Create a new version of a secret or add a new secret to a namespace.

**Endpoint:** `POST /redenv/<namespace>/<key>`

- `<namespace>`: The namespace in which the secret will be stored.
- `<key>`: The key or name of the secret.
- Request Body: The secret data in valid JSON format.

**Example:**

```
POST /redenv/my_namespace/my_secret

Request Body:
{
  "username": "admin",
  "password": "s3cr3tp@ssw0rd"
}
```

This request will create a new version of the secret named `my_secret` in the `my_namespace` namespace with the provided JSON data.

#### 3. Delete a Secret

Delete a specific version of a secret stored in a namespace.

**Endpoint:** `DELETE /redenv/<namespace>/<key>:<version>`

- `<namespace>`: The namespace from which the secret will be deleted.
- `<key>`: The key or name of the secret.
- `<version>` (optional): The version of the secret to delete. If omitted, the API will delete the latest version.

**Example:**

```
DELETE /redenv/my_namespace/my_secret:1
```

This request will delete version 1 of the secret named `my_secret` in the `my_namespace` namespace.

### Notes

- The Redenv API is designed to run locally on a user's computer or on-premises; we are thinking of providing also an online service, let us know if you would like this!
- For local deployments, the Redenv API can utilize UNIX sockets for communication.
- Secrets can be any valid JSON, allowing flexibility in the type of data that can be stored.
- The namespace and key in the URLs should be valid Rust string characters, except for the colon (":") which is reserved as the separator between the key and version.
- DELETE implements a soft deleting by default, meaning the key is transfered to a trash storage, and it can be easily retrieved with a local access.
- The Redenv API is implemented in Rust.

