use crate::errors::RedenvError;
use fs_extra::dir::move_dir;
use fs_extra::dir::CopyOptions;
use serde::{Deserialize, Serialize};
use std::fs::remove_dir_all;

use crate::functions::filesystem::{get_key_path, get_key_trash};
use crate::models::secret::Secret;

#[derive(Debug, Default, Serialize, Deserialize, Clone)]
pub struct Namespace {
    pub name: String,
    pub secrets: Vec<Secret>,
}

impl Namespace {
    pub fn new(name: String) -> Self {
        Self {
            name,
            secrets: Vec::new(),
        }
    }
    pub fn push(&mut self, secret: Secret) {
        self.secrets.push(secret);
    }
    pub fn delete(&mut self, secret: Secret) -> Result<(), RedenvError> {
        self.secrets.retain(|item| item.key != secret.key);
        let source_dir = get_key_path(&self.name, &secret.key);
        let target_dir = get_key_trash(&self.name, &secret.key)?;

        // Move the directory
        let options = CopyOptions::new().copy_inside(true);

        move_dir(&source_dir, target_dir, &options).map_err(|_| RedenvError::MoveToTrashError)?;

        remove_dir_all(source_dir).ok().unwrap_or(());
        Ok(())
    }
}

#[cfg(test)]
mod secret_tests {
    use crate::models::namespace::Namespace;
    use crate::models::secret::Secret;
    #[test]
    fn nemespace_creation() {
        let secret1 = Secret {
            key: "secret1".to_string(),
            version: "v1".to_string(),
            value: serde_json::json!({"key1": "value1"}),
        };
        let secret2 = Secret {
            key: "secret1".to_string(),
            version: "v1".to_string(),
            value: serde_json::json!({"key2": "value2"}),
        };
        let _namespace = Namespace {
            name: "my_namespace".to_string(),
            secrets: vec![secret1, secret2],
        };
    }

    #[test]
    fn namespace_methods() {
        let secret = Secret {
            key: "secret".to_string(),
            version: "v1".to_string(),
            value: serde_json::json!({"key1": "value1"}),
        };

        let mut namespace = Namespace::new("my_namespace".to_string());

        namespace.push(secret);
    }

    #[test]
    fn json_serialization() -> Result<(), serde_json::Error> {
        let mut namespace = Namespace::new("my_namespace".to_string());
        namespace.push(Secret::new(
            "secret".to_string(),
            "v1".to_string(),
            serde_json::json!({"key1": "value1"}),
        ));

        // Serialize to JSON
        let json = serde_json::to_string(&namespace)?;

        // Deserialize from JSON
        let _deserialized_namespace: Namespace = serde_json::from_str(&json)?;

        Ok(())
    }
}
