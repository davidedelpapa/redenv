use serde::{Deserialize, Serialize};
use std::error::Error;

use std::fs::{create_dir_all, File};
use std::io::Write;
use std::path::PathBuf;

use crate::errors::RedenvError;
use crate::functions::filesystem::get_key_path;
use crate::models::history::{KeyHistory, VersionHistory};

#[derive(Debug, Default, Serialize, Deserialize, Clone)]
pub struct Secret {
    pub key: String,
    pub version: String,
    pub value: serde_json::Value,
}

impl PartialEq for Secret {
    fn eq(&self, other: &Self) -> bool {
        self.key == other.key && self.version == other.version
    }
}

impl PartialEq<Secret> for &Secret {
    fn eq(&self, other: &Secret) -> bool {
        *self == other
    }
}

impl Secret {
    /// Secret::new() constructor
    pub fn new(key: String, version: String, value: serde_json::Value) -> Self {
        Self {
            key,
            version,
            value,
        }
    }

    /// Store a Secret to the filesystem
    pub fn store(&self, namespace: &str) -> Result<(), Box<dyn Error>> {
        let key = &self.key;
        let version = &self.version;

        let key_folder = get_key_path(namespace, key);
        create_dir_all(&key_folder)?; // Creates the path if it doesn't exist

        // Create the secret file with the version as the filename
        let secret_file = key_folder.join(format!("{}.bin", version));
        let file = File::create(secret_file)?;
        serde_cbor::to_writer(file, &self)?;

        // Create or update the current version file
        let current_file = key_folder.join("current.txt");
        let mut current_version_file = File::create(current_file)?;
        current_version_file.write_all(version.as_bytes())?;

        // Create or update the history file
        let history_file = key_folder.join("history.json");
        let history = KeyHistory::from_file(&history_file)?;

        let mut key_history = KeyHistory {
            current: version.to_string(),
            history: history.history,
        };

        let version_history = VersionHistory {
            version: version.to_string(),
            timestamp: get_current_timestamp(),
        };

        key_history.history.push(version_history);

        let history_file = File::create(history_file)?;
        serde_cbor::to_writer(history_file, &key_history)?;

        Ok(())
    }

    /// Retrieves a Secret from the Filesystem
    pub fn from_file(secret_file: String) -> Result<Option<Self>, RedenvError> {
        let secret_file = PathBuf::from(secret_file);
        if secret_file.exists() {
            let file = File::open(secret_file).map_err(|_| RedenvError::NoSecretError)?;
            let secret: Secret =
                serde_cbor::from_reader(file).map_err(|_| RedenvError::CorruptedSecretError)?;
            return Ok(Some(secret));
        }
        Ok(None)
    }
}

// TODO
fn get_current_timestamp() -> String {
    format!("{:?}", chrono::offset::Utc::now())
}

#[cfg(test)]
mod secret_tests {
    use crate::models::secret::Secret;
    #[test]
    fn secret_creation() {
        let secret1 = Secret {
            key: "secret1".to_string(),
            version: "v1".to_string(),
            value: serde_json::json!({"key1": "value1"}),
        };
    }

    #[test]
    fn secret_methods() {
        let _secret = Secret::new(
            "secret".to_string(),
            "v1".to_string(),
            serde_json::json!({"key1": "value1"}),
        );
    }
    #[test]
    fn json_serialization() -> Result<(), serde_json::Error> {
        let secret = Secret::new(
            "secret".to_string(),
            "v1".to_string(),
            serde_json::json!({"key1": "value1"}),
        );

        // Serialize to JSON
        let json = serde_json::to_string(&secret)?;

        // Deserialize from JSON
        let _deserialized_secret: Secret = serde_json::from_str(&json)?;

        Ok(())
    }
}
