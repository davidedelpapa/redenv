use serde::{Deserialize, Serialize};
use std::error::Error;
use std::fs::File;
use std::path::Path;

#[derive(Serialize, Deserialize)]
pub struct KeyHistory {
    pub current: String,
    pub history: Vec<VersionHistory>,
}

#[derive(Serialize, Deserialize)]
pub struct VersionHistory {
    pub version: String,
    pub timestamp: String, // Or any appropriate timestamp representation
}

impl KeyHistory {
    pub fn from_file(history_file: &Path) -> Result<Self, Box<dyn Error>> {
        if history_file.exists() {
            let file = File::open(history_file)?;
            let history: KeyHistory = serde_cbor::from_reader(file)?;
            Ok(history)
        } else {
            Ok(Self {
                current: "".to_string(),
                history: vec![],
            })
        }
    }
}
