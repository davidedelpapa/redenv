#![allow(unused_variables)]
#![allow(dead_code)]

#[cfg(all(feature = "tcp", feature = "socket"))]
use futures::future;
#[cfg(feature = "socket")]
#[cfg(windows)]
use tokio::net::windows::named_pipe::ServerOptions;
#[cfg(feature = "socket")]
#[cfg(unix)]
use tokio::net::UnixListener;
#[cfg(feature = "socket")]
#[cfg(unix)]
use tokio_stream::wrappers::UnixListenerStream;

use warp::Filter;

mod errors;
mod functions;
mod models;
mod routes;

#[cfg(test)]
mod tests;

#[tokio::main]
async fn main() {
    let api_get = routes::api_get();
    let api_post = routes::api_post();
    let api_delete = routes::api_delete();

    let api = warp::path("redenv").and(api_get.or(api_post).or(api_delete));

    #[cfg(feature = "secure")]
    let secure_api_get = routes::secure_api_get();
    #[cfg(feature = "secure")]
    let secure_api_post = routes::secure_api_post();
    #[cfg(feature = "secure")]
    let secure_api_delete = routes::secure_api_delete();

    #[cfg(feature = "secure")]
    let secure_api =
        warp::path("sredenv").and(secure_api_get.or(secure_api_post).or(secure_api_delete));

    #[cfg(not(feature = "secure"))]
    let routes = api;
    #[cfg(feature = "secure")]
    let routes = api.or(secure_api);

    let host = "127.0.0.1:4000"; // TODO create options
    println!("Running on http://{}", host);

    cfg_if::cfg_if! {
        if #[cfg(all(feature = "tcp", feature = "socket"))] {
            // Spawn both TCP and socket handlers
            #[cfg(unix)]
            {
                let listener = UnixListener::bind("/tmp/redenv.sock").unwrap();   // TODO put socket address in env options
                                                                                // TODO away with unwrap and log this like below
                let incoming = UnixListenerStream::new(listener);
                let socket_warp = warp::serve(routes.clone())
                    .serve_incoming(incoming);
                let (_tcp_addr, tcp_warp) = warp::serve(routes)
                    .bind_ephemeral(host.parse::<std::net::SocketAddr>().unwrap());

                future::join(socket_warp, tcp_warp).await;
            }
            #[cfg(windows)]
            {
                eprintln!("No Named pipes support yet");
                return;
            }

        } else if #[cfg(feature = "socket")] {
            // Only spawn socket handler
            #[cfg(unix)]
            {
                let listener = UnixListener::bind("/tmp/redenv.sock").unwrap();   // TODO put socket address in env options
                                                                                // TODO away with unwrap and log this
                let incoming = UnixListenerStream::new(listener);
                let socket_warp = warp::serve(routes.clone())
                    .run_incoming(incoming)
                    .await;
            }

            #[cfg(windows)]
            {
                eprintln!("No Named pipes support yet");
                return;
            }

        } else {
            // Only spawn tcp handler
            // This is default even if the tcp features is not chosen;
            //  in fact it is here to prepare for tls support, so regulat tcp with no tls will be default
            warp::serve(routes)
                .run(host.parse::<std::net::SocketAddr>().unwrap())
                .await;
        }
    }
}
