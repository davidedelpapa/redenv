use crate::{errors::RedenvError, models::secret::Secret};
use warp::{Rejection, Reply};

use super::utils::split_key_and_version;

#[cfg(feature = "secure")]
use super::utils::extract_aes_key;
#[cfg(feature = "secure")]
use super::utils::unimplemented;
#[cfg(feature = "secure")]
use warp::http::HeaderMap;

// Route for storing a secret in a namespace and key
pub async fn store_secret(
    namespace: String,
    key_version: String,
    value: serde_json::Value,
) -> Result<impl Reply, Rejection> {
    let (key, version) = split_key_and_version(&key_version);
    let version = get_version(version);

    let secret = Secret::new(key.to_string(), version, value);
    secret
        .store(namespace.as_str())
        .map_err(|_| RedenvError::StoreError)?;
    Ok(warp::reply::json(&secret))
}

// Secure API route for storing a secret in a namespace and key
#[cfg(feature = "secure")]
pub async fn store_secure_secret(
    namespace: String,
    key_version: String,
    headers: HeaderMap,
    value: serde_json::Value,
) -> Result<impl Reply, Rejection> {
    let (key, version) = split_key_and_version(&key_version);
    let aes_key = extract_aes_key(&headers)?;

    // Implement your logic for storing the secure secret here
    unimplemented()
}

// TODO
fn get_version(ver: Option<&str>) -> String {
    match ver {
        Some(v) => v.to_string(),
        None => "0".to_string(), // Handle versioning properly
    }
}
