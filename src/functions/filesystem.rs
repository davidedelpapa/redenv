use std::env;
use std::fs::{create_dir_all, read_to_string};
use std::path::PathBuf;

use crate::errors::RedenvError;

pub fn get_current_txt(
    namespace: impl ToString,
    key: impl ToString,
) -> Result<String, RedenvError> {
    let key_folder = get_key_path(namespace, key);
    if !key_folder.exists() {
        return Err(RedenvError::NoSecretError);
    }

    let current_file = key_folder.join("current.txt");
    read_to_string(current_file).map_err(|_| RedenvError::GetCurrentError)
}

// Returns the construction of the path for teh secret.
// It might not exist in reality, so check with .exists()
// Even if it does not exist, it is useful, nonetheless,
//  because it can be constructed with std::fs::create_dir_all()
pub fn get_key_path(namespace: impl ToString, key: impl ToString) -> PathBuf {
    let current = env::current_dir().unwrap(); // TODO
    let data_folder = current.join("data"); // TODO
    let namespace_folder = data_folder.join(namespace.to_string());
    namespace_folder.join(key.to_string())
}

// Needed to move a key to the trash
pub fn get_key_trash(namespace: impl ToString, key: impl ToString) -> Result<PathBuf, RedenvError> {
    let trash_folder = get_trash();
    let namespace_folder = trash_folder?.join(namespace.to_string());
    Ok(namespace_folder.join(key.to_string()))
}

pub fn get_trash() -> Result<PathBuf, RedenvError> {
    let current = env::current_dir().unwrap(); // TODO
    let trash = current.join("data_trash");
    create_dir_all(&trash).map_err(|_| RedenvError::GetTrashError)?;
    Ok(trash) // TODO
}
