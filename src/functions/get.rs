use serde::Serialize;
use warp::{Rejection, Reply};

use super::{
    filesystem::{get_current_txt, get_key_path},
    utils::split_key_and_version,
};
use crate::{errors::RedenvError, models::secret::Secret};

#[cfg(feature = "secure")]
use super::utils::extract_aes_key;
#[cfg(feature = "secure")]
use super::utils::unimplemented;
#[cfg(feature = "secure")]
use warp::http::HeaderMap;

#[derive(Serialize)]
pub struct GetResponse {
    pub secret: Secret,
}

#[derive(Serialize)]
pub struct FailedGetResponse {
    pub error: String,
}

// Route for getting a specific secret by namespace, key, and version
pub async fn get_secret(namespace: String, key_version: String) -> Result<impl Reply, Rejection> {
    let (key, version) = split_key_and_version(&key_version);

    let secret_path = get_path_for_secret(namespace, key, version)?;
    match Secret::from_file(secret_path).map_err(|_| RedenvError::NoSecretError)? {
        Some(secret) => Ok(warp::reply::json(&GetResponse { secret })),
        None => Ok(warp::reply::json(&FailedGetResponse {
            error: "No secret found".to_string(),
        })),
    }
}

// Secure API route for getting a specific secret by namespace, key, and version
#[cfg(feature = "secure")]
pub async fn get_secure_secret(
    namespace: String,
    key_version: String,
    headers: HeaderMap,
) -> Result<impl Reply, Rejection> {
    let (key, version) = split_key_and_version(&key_version);
    let aes_key = extract_aes_key(&headers)?;

    // Implement your logic for retrieving the secure secret here
    unimplemented()
}

fn get_path_for_secret(
    namespace: String,
    key: &str,
    version: Option<&str>,
) -> Result<String, RedenvError> {
    let key_folder = get_key_path(&namespace, key);
    if !key_folder.exists() {
        return Err(RedenvError::NoSecretError);
    }
    let version = match version {
        Some(v) => format!("{}.bin", v), // TODO
        None => {
            let current_version = get_current_txt(namespace, key)?;
            format!("{}.bin", current_version) // TODO
        }
    };
    key_folder
        .join(version)
        .into_os_string()
        .into_string()
        .map_err(|_| RedenvError::NoSecretError)
}
