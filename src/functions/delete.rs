use super::utils::split_key_and_version;
use crate::models::namespace::Namespace;
use crate::models::secret::Secret;
use serde::Serialize;
use serde_json::json;
use warp::{Rejection, Reply};

#[cfg(feature = "secure")]
use super::utils::extract_aes_key;
#[cfg(feature = "secure")]
use super::utils::unimplemented;
#[cfg(feature = "secure")]
use warp::http::HeaderMap;

#[derive(Serialize)]
pub struct DeleteResponse {
    pub deleted: String,
}

#[derive(Serialize)]
pub struct FailedDeleteResponse {
    pub error: String,
}

// Route for deleting a secret in a namespace, key, and version
pub async fn delete_secret(
    namespace: String,
    key_version: String,
) -> Result<impl Reply, Rejection> {
    let (key, version) = split_key_and_version(&key_version);

    // TODO retrieve instead of constructing
    let mut namespace = Namespace::new(namespace);
    let secret = Secret::new(key.to_string(), "".to_string(), json!(&false));
    match namespace.delete(secret) {
        Ok(_) => Ok(warp::reply::json(&DeleteResponse {
            deleted: key.to_string(),
        })),
        Err(_) => Ok(warp::reply::json(&FailedDeleteResponse {
            error: "No Secret found".to_string(),
        })),
    }
}

// Secure API route for deleting a secret in a namespace, key, and version
#[cfg(feature = "secure")]
pub async fn delete_secure_secret(
    namespace: String,
    key_version: String,
    headers: HeaderMap,
) -> Result<impl Reply, Rejection> {
    let (key, version) = split_key_and_version(&key_version);
    let aes_key = extract_aes_key(&headers)?;

    // Implement your logic for deleting the secure secret here
    unimplemented()
}
