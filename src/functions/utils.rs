use warp::{http::HeaderMap, Rejection, Reply};

use crate::errors::RedenvError;

// Unimplemented version for warp routes
pub fn unimplemented() -> Result<impl Reply, Rejection> {
    Ok(warp::reply::json(&"test: OK".to_string()))
}

// Function to split key and version from the given string
pub fn split_key_and_version(key_version: &str) -> (&str, Option<&str>) {
    let mut iter = key_version.splitn(2, ':');
    let key = iter.next().unwrap();
    let version = iter.next();
    (key, version)
}

// Function to extract AES Key from the Headers
pub fn extract_aes_key(headers: &HeaderMap) -> Result<String, warp::Rejection> {
    if let Some(header_value) = headers.get("X-AES-Key") {
        if let Ok(aes_key) = header_value.to_str() {
            return Ok(aes_key.to_owned());
        }
    }

    Err(warp::reject::custom(RedenvError::MissingAESKey))
}
