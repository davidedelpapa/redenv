use serde_json::json;

use crate::functions::delete::delete_secret;
use crate::functions::store::store_secret;
use crate::routes::{api_delete, api_get, api_post};
#[cfg(feature = "secure")]
use crate::routes::{secure_api_delete, secure_api_get, secure_api_post};

#[tokio::test]
async fn test_get() {
    // Create your server routes
    let route = api_get();
    let _ = store_secret(
        "mynamespace_get".to_string(),
        "mykey".to_string(),
        json!("myversion"),
    )
    .await;
    assert!(
        warp::test::request()
            .path("/mynamespace_get/mykey:myversion")
            .matches(&route)
            .await
    );
    let _ = delete_secret("mynamespace_get".to_string(), "mykey".to_string()).await;
}

#[tokio::test]
async fn test_post() {
    // Create your server routes
    let route = api_post();

    assert!(
        warp::test::request()
            .method("POST")
            .json(&true)
            .path("/mynamespace_post/mykey:myversion")
            .matches(&route)
            .await
    );
    let _ = delete_secret("mynamespace_post".to_string(), "mykey".to_string()).await;
}

#[tokio::test]
async fn test_delete() {
    // Create your server routes
    let route = api_delete();

    let _ = store_secret(
        "mynamespace_delete".to_string(),
        "mykey".to_string(),
        json!("myversion"),
    )
    .await;

    assert!(
        warp::test::request()
            .method("DELETE")
            .path("/mynamespace_delete/mykey:myversion")
            .matches(&route)
            .await
    );
}

#[cfg(feature = "secure")]
#[tokio::test]
async fn test_secure_get() {
    // Create your server routes
    let route = secure_api_get();

    assert!(
        warp::test::request()
            .header("X-AES-Key", "myAesKey")
            .path("/mynamespace/mykey:myversion")
            .matches(&route)
            .await
    );
}

#[cfg(feature = "secure")]
#[tokio::test]
async fn test_secure_post() {
    // Create your server routes
    let route = secure_api_post();

    assert!(
        warp::test::request()
            .method("POST")
            .header("X-AES-Key", "myAesKey")
            .json(&true)
            .path("/mynamespace/mykey:myversion")
            .matches(&route)
            .await
    );
}

#[cfg(feature = "secure")]
#[tokio::test]
async fn test_secure_delete() {
    // Create your server routes
    let route = secure_api_delete();

    assert!(
        warp::test::request()
            .method("DELETE")
            .header("X-AES-Key", "myAesKey")
            .path("/mynamespace/mykey:myversion")
            .matches(&route)
            .await
    );
}
