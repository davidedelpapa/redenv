use warp::{Filter, Rejection, Reply};

use crate::functions::delete::delete_secret;
use crate::functions::get::get_secret;
use crate::functions::store::store_secret;

#[cfg(feature = "secure")]
use crate::functions::delete::delete_secure_secret;
#[cfg(feature = "secure")]
use crate::functions::get::get_secure_secret;
#[cfg(feature = "secure")]
use crate::functions::store::store_secure_secret;

pub fn api_get() -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!(String / String)
        .and(warp::get())
        .and_then(get_secret)
}

pub fn api_post() -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!(String / String)
        .and(warp::post())
        .and(warp::body::json())
        .and_then(store_secret)
}

pub fn api_delete() -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!(String / String)
        .and(warp::delete())
        .and_then(delete_secret)
}

#[cfg(feature = "secure")]
pub fn secure_api_get() -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!(String / String)
        .and(warp::get())
        .and(warp::header::headers_cloned())
        .and_then(get_secure_secret)
}

#[cfg(feature = "secure")]
pub fn secure_api_post() -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!(String / String)
        .and(warp::post())
        .and(warp::header::headers_cloned())
        .and(warp::body::json())
        .and_then(store_secure_secret)
}

#[cfg(feature = "secure")]
pub fn secure_api_delete() -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!(String / String)
        .and(warp::delete())
        .and(warp::header::headers_cloned())
        .and_then(delete_secure_secret)
}
